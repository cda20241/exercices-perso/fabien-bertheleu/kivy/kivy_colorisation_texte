from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput



ROUGE=(2, 0, 0, 1)
BLEU=(0, 0.5019607843137255, 1, 1)
VERT=(0.4, 0.8, 0, 1)
JAUNE=(3.0, 2.7, 0.3, 2.0)
ROSE=(3, 0.3 , 0.9, 1)
MARRON=(1.6, 0.6, 0.3, 1)


class ExempleApp(App):
    def build(self):
        # Je créé la box1 avec son nombre de rangées, son orientation, etc.
        self.box1 = BoxLayout(orientation='horizontal')

        # J'ai créé la box, maintenant, j'ajoute 2 grids dedans
        self.grid_gauche = GridLayout(rows=2)
        self.grid_droite = GridLayout(cols=2)

        # Je créé un label avec du texte que j'ajoute à la grid gauche
        self.label = Label(text="Entrez du texte")
        self.grid_gauche.add_widget(self.label)

        # Je créé un input
        self.input = TextInput(text="Saisissez votre nouveau texte ici")
        self.input.bind(text=self.texte) #je rajoute un bind qui appelle la fonction texte et qui change le texte du label
        self.grid_gauche.add_widget(self.input)

        # J'appelle la fonction bouton pour créer les boutons et les ajouter à la grille droite
        self.bouton("Rouge", ROUGE)
        self.bouton("Bleu", BLEU)
        self.bouton("Jaune", JAUNE)
        self.bouton("Rose", ROSE)
        self.bouton("Marron", MARRON)
        self.bouton("Vert", VERT)

        # J'ajoute les widgets à la boîte principale
        self.box1.add_widget(self.grid_gauche)
        self.box1.add_widget(self.grid_droite)

        return self.box1

        # Fonction pour créer un bouton avec un label et une couleur spécifique

    def bouton(self, texte, couleur):
        button = Button(text=texte, background_color=couleur)
        # Lien de la fonction changeCouleur à l'événement on_press du bouton
        button.bind(on_press=self.changeCouleur)
        # Ajout du bouton à la grille droite
        self.grid_droite.add_widget(button)

        # Fonction appelée lorsqu'un bouton est pressé, change la couleur du texte du label

    def changeCouleur(self, button):
        self.label.color = button.background_color

    # une fonction texte qui permet de créer une instance de texte. Là où cette finction est utilisée, cela change
    #le texte du label.
    def texte(self, instance, args):
        self.label.text = instance.text


if __name__ == '__main__':
        ExempleApp().run()