from holoviews import Layout
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput



ROUGE=(1, 0, 0, 1)
BLEU=(0, 0.5019607843137255, 1, 1)
VERT=(0.4, 0.8, 0, 1)
JAUNE=(1, 1, 0, 1)
ROSE=(1, 0.6 , 0.8, 1)
MARRON=(0.4, 0.3, 0, 1)
class Exemple(GridLayout):
    def build(self):
        self.layout = BoxLayout(orientation='vertical', spacing=10, padding=50)
        self.Un_Label()
        self.Un_Input()
        self.Boutton_rouge()
        self.Boutton_bleu()
        self.Boutton_vert()
        self.Boutton_jaune()
        self.Boutton_rose()
        self.Boutton_marron()


    def Boutton_rouge(self):
        Boutton_rouge = Button(text="Rouge")
        Boutton_rouge.pos = 0, 200
        Boutton_rouge.background_color =ROUGE
        self.add_widget(Boutton_rouge)

    def Boutton_bleu(self):
        Boutton_bleu = Button(text="Bleu")
        Boutton_bleu.pos = 0, 400
        Boutton_bleu.background_color = BLEU
        self.add_widget(Boutton_bleu)

    def Boutton_vert(self):
        Boutton_vert = Button(text="Vert")
        Boutton_vert.pos = 200, 200
        Boutton_vert.background_color = "lime"
        self.add_widget(Boutton_vert)

    def Boutton_jaune(self):
        Boutton_jaune = Button(text="Jaune")
        Boutton_jaune.pos = 200, 400
        Boutton_jaune.background_color = JAUNE
        self.add_widget(Boutton_jaune)

    def Boutton_rose(self):
        Boutton_rose = Button(text="Rose")
        Boutton_rose.pos = 400, 400
        Boutton_rose.background_color = ROSE
        self.add_widget(Boutton_rose)

    def Boutton_marron(self):
        Boutton_marron = Button(text="Marron")
        Boutton_marron.pos = 400, 200
        Boutton_marron.background_color = MARRON
        self.add_widget(Boutton_marron)


    def Un_Input(self):
        # On cree un Input avec des ses proprietes:
        self.Input1 = TextInput(text="Texte initial", font_size=30)
        # On ajoute des proprietes (une largeur de 50% et une hauteur de 20%)
        self.Input1.size_hint_x = 0.5
        self.Input1.size_hint_y = 0.2
        # On l'ajoute au layout principal:
        self.add_widget(self.Input1)

    def Un_Label(self):
        # On cree un label avec toutes ses proprietes:
        self.Label1 = Label(text="Entrez du Texte", font_size=30, color="red")
        self.Label1.pos = 400, 0
        # On l'ajoute au layout principal:
        self.add_widget(self.Label1)



class ExempleApp(App):
    def build(self):
        Layout = Exemple()
        Layout.build()
        return Layout


if __name__ == '__main__':
    ExempleApp().run()